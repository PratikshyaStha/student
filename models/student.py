from odoo import models, fields, api, exceptions
import logging

_logger = logging.getLogger(__name__)


class StudentStudent(models.Model):
    _name = 'student.student'
    _description = 'Student Information'

    name = fields.Char(string="Name")
    user_id = fields.Many2one('res.users', 'User ID')
    student_code = fields.Char('Student Code')
    contact_phone1 = fields.Char('Phone no.', )
    contact_mobile1 = fields.Char('Mobile no', )
    roll_no = fields.Integer('Roll No.', readonly=True)
    middle = fields.Char('Middle Name')
    last = fields.Char('Surname')
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')],
                              'Gender', states={'done': [('readonly', True)]})
    date_of_birth = fields.Date('BirthDate', required=True,
                                states={'done': [('readonly', True)]})
    maritual_status = fields.Selection([('unmarried', 'Unmarried'),
                                        ('married', 'Married')],
                                       'Marital Status',
                                       states={'done': [('readonly', True)]})
    doctor = fields.Char('Doctor Name', states={'done': [('readonly', True)]})
    designation = fields.Char('Designation')
    doctor_phone = fields.Char('Phone')
    blood_group = fields.Char('Blood Group')
    height = fields.Float('Height', help="Hieght in C.M")
    weight = fields.Float('Weight', help="Weight in K.G")
    eye = fields.Boolean('Eyes')
    ear = fields.Boolean('Ears')
    nose_throat = fields.Boolean('Nose & Throat')
    respiratory = fields.Boolean('Respiratory')
    cardiovascular = fields.Boolean('Cardiovascular')
    neurological = fields.Boolean('Neurological')
    muskoskeletal = fields.Boolean('Musculoskeletal')
    dermatological = fields.Boolean('Dermatological')
    blood_pressure = fields.Boolean('Blood Pressure')
    remark = fields.Text('Remark', states={'done': [('readonly', True)]})
    state = fields.Selection([('draft', 'Draft'),
                              ('done', 'Done'),
                              ('terminate', 'Terminate'),
                              ('cancel', 'Cancel'),
                              ('alumni', 'Alumni')],
                             'State', readonly=True, default="draft")
    terminate_reason = fields.Text('Reason')
    active = fields.Boolean(default=True)


